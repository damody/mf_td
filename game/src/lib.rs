//! Game project.
#![allow(warnings)]
#![allow(unused)]

use fyrox::asset::untyped::ResourceKind;
use fyrox::core::impl_component_provider;
use fyrox::material::{Material, MaterialResource};
use fyrox::{
    event::Event,
    core::{
        pool::Handle,
        algebra::{Matrix4, Vector3},
        color::Color,
        reflect::prelude::*,
        uuid::{uuid, Uuid},
        visitor::prelude::*,
        TypeUuidProvider,
        },
    gui::{
        message::{MessageDirection, UiMessage},
        text::{TextBuilder, TextMessage},
        widget::WidgetBuilder,
        UiNode,
    },
    resource::texture::Texture,
    engine::GraphicsContext,
    plugin::{Plugin, PluginConstructor, PluginContext, PluginRegistrationContext},
    scene::{base::BaseBuilder, camera::CameraBuilder, dim2::rectangle::RectangleBuilder, graph::Graph, 
        node::Node,
        mesh::{
            surface::{SurfaceBuilder, SurfaceData, SurfaceSharedData},
            MeshBuilder,
        }, transform::TransformBuilder, Scene},
};

use serde_derive::{Deserialize, Serialize};
use std::path::Path;

pub mod config;
use crate::config::server_config::CONFIG;
use crossbeam_channel::{bounded, select, tick, Receiver, Sender};
use rumqttc::{Client, Connection, MqttOptions, QoS};
use serde_json::{self, json};
use failure::{err_msg, Error};
use std::{
    fs, i32, io,
    ops::{Deref, DerefMut},
    sync::{mpsc, Arc},
    thread,
    time::{Duration, Instant, SystemTime},
};
use regex::Regex;
use log::{debug, error, info, trace, warn};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PlayerData {
    pub name: String,
    pub t: String,
    pub a: String,
    pub d: serde_json::Value,
}


pub struct GameConstructor;

fn create_mqtt_client(
    server_addr: String,
    server_port: String,
    client_id: String,
    sub: bool,
) -> Result<(Client, Connection), Error> {
    let mut mqtt_options = MqttOptions::new(
        client_id.as_str(),
        server_addr.as_str(),
        server_port.parse::<u16>()?,
    );
    mqtt_options.set_keep_alive(Duration::from_secs(10));
    mqtt_options.set_request_channel_capacity(10000);
    mqtt_options.set_clean_session(true);
    let (mut mqtt_cli, mut connection) = Client::new(mqtt_options.clone(), 100000);
    if sub {
        mqtt_cli.subscribe("td/+/server", QoS::AtMostOnce).unwrap();
    }
    Ok((mqtt_cli, connection))
}
fn generate_client_id() -> String {
    let s = format!("td_{}", Uuid::new_v4().as_simple());
    (&s[..3]).to_string()
}
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MqttMsg {
    pub topic: String,
    pub msg: String,
    pub time: SystemTime,
}
impl PluginConstructor for GameConstructor {
    fn register(&self, _context: PluginRegistrationContext) {
        // Register your scripts here.
    }

    fn create_instance(&self, scene_path: Option<&str>, context: PluginContext) -> Box<dyn Plugin> {
        let server_addr = CONFIG.SERVER_IP.clone();
        let server_port = CONFIG.SERVER_PORT.clone();
        let client_id = CONFIG.CLIENT_ID.clone();
        let mqtt_url = "tcp://".to_owned() + &server_addr + ":" + &server_port;
        let (mqtx, rx): (Sender<MqttMsg>, Receiver<MqttMsg>) = bounded(10000);
        let mqrx = pub_mqtt_loop(
            server_addr.clone(),
            server_port.clone(),
            rx.clone(),
            client_id.clone(),
        ).unwrap();        
        Box::new(Game::new(scene_path, context, mqrx, mqtx))
    }
}

pub struct Game {
    scene: Handle<Scene>,
    debug_text: Handle<UiNode>,
    mqrx: Receiver<PlayerData>,
    mqtx: Sender<MqttMsg>,
}

impl Game {
    pub fn new(scene_path: Option<&str>, mut context: PluginContext, mqrx: Receiver<PlayerData>, mqtx: Sender<MqttMsg>) -> Self {
        //context.async_scene_loader.request(scene_path.unwrap_or("data/scene.rgs"));
        let mut scene = create_camera(&mut context);
        for i in 0..10 {
            for j in 0..10 {
                create_tower(&mut context,&mut scene.graph, Vector3::new(i as f32, j as f32, 0.0));
            }
        }
        let res = context.scenes.add(scene);
        //create_scene(&mut context);
        let debug_text =
            TextBuilder::new(WidgetBuilder::new()).build(&mut context.user_interface.build_ctx());
        Self {
            scene: res,
            debug_text,
            mqrx: mqrx,
            mqtx: mqtx,
        }
    }
}

fn create_scene(ctx: &mut PluginContext) {
    // Create a new scene first.
    let mut scene = Scene::new();

    // Add a camera (without it you won't see anything).
    CameraBuilder::new(
        BaseBuilder::new().with_local_transform(
            TransformBuilder::new()
                .with_local_position(Vector3::new(0.0, 0.0, -2.0))
                .build(),
        ),
    )
    .build(&mut scene.graph);

    // Create a cube.
    MeshBuilder::new(BaseBuilder::new())
        .with_surfaces(vec![SurfaceBuilder::new(SurfaceSharedData::new(
            SurfaceData::make_cube(Matrix4::identity()),
        ))
        .build()])
        .build(&mut scene.graph);

    // Add the scene to the engine.
    let scene_handle = ctx.scenes.add(scene);
}

fn create_camera(ctx: &mut PluginContext) -> Scene {
    let mut scene = Scene::new();
    // Add a camera (without it you won't see anything).
    CameraBuilder::new(
        BaseBuilder::new().with_local_transform(
            TransformBuilder::new()
                .with_local_position(Vector3::new(0.0, 0.0, -20.0))
                .build(),
        ),
    )
    .build(&mut scene.graph);
    
    scene
}

fn create_creep(ctx: &mut PluginContext, graph: &mut Graph, pos: Vector3<f32>) -> Creep {
    // Create a picture.
    let texture = ctx.resource_manager.request::<Texture>("data/sprites/tower_tiles/Red tower/block(1).png");
    let mut material = Material::standard();
    let _ = material.set_texture(&"diffuseTexture".into(), Some(texture.clone()));
    let handle_material = MaterialResource::new_ok(ResourceKind::Embedded, material);
    let res = RectangleBuilder::new(
        BaseBuilder::new().with_local_transform(
            TransformBuilder::new()
                // Size of the rectangle is defined only by scale.
                .with_local_scale(Vector3::new(2., 2., 1.))
                .with_local_position(pos)
                .build(),
        ),
    )
    .with_color(Color::RED)
    .with_material(handle_material)
    .build(graph);
    let creep = Creep {
        sprite: res,
        move_speed: 10.,
    };
    creep
}

fn create_tower(ctx: &mut PluginContext, graph: &mut Graph, pos: Vector3<f32>) -> Handle<Node> {
    // Create a picture.
    let texture = ctx.resource_manager.request::<Texture>("data/sprites/tower_tiles/Red tower/block(11).png");
    let mut material = Material::standard();
    let _ = material.set_texture(&"diffuseTexture".into(), Some(texture.clone()));
    let handle_material = MaterialResource::new_ok(ResourceKind::Embedded, material);
    let res = RectangleBuilder::new(
        BaseBuilder::new().with_local_transform(
            TransformBuilder::new()
                // Size of the rectangle is defined only by scale.
                .with_local_scale(Vector3::new(2., 2., 1.))
                .with_local_position(pos)
                .build(),
        ),
    )
    .with_color(Color::RED)
    .with_material(handle_material)
    .build(graph);
    res
}

impl Plugin for Game {
    fn on_deinit(&mut self, _context: PluginContext) {
        // Do a cleanup here.
    }

    fn update(&mut self, context: &mut PluginContext) {
        // Add your global update code here.
        if let GraphicsContext::Initialized(graphics_context) = context.graphics_context {
            context.user_interface.send_message(TextMessage::text(
                self.debug_text,
                MessageDirection::ToWidget,
                format!("{}", graphics_context.renderer.get_statistics()),
            ));
        }

    }

    fn on_os_event(
        &mut self,
        _event: &Event<()>,
        _context: PluginContext,
    ) {
        // Do something on OS event here.
    }

    fn on_ui_message(
        &mut self,
        _context: &mut PluginContext,
        _message: &UiMessage,
    ) {
        // Handle UI events here.
    }
    
    fn on_scene_begin_loading(&mut self, path: &Path, ctx: &mut PluginContext) {
        if self.scene.is_some() {
            ctx.scenes.remove(self.scene);
        }
    }

    fn on_scene_loaded(
        &mut self,
        path: &Path,
        scene: Handle<Scene>,
        data: &[u8],
        context: &mut PluginContext,
    ) {    
        self.scene = scene;
    }
}


#[derive(Visit, Reflect, Debug, Clone)]
struct Creep {
    sprite: Handle<Node>,
    move_speed: f32,

}

impl_component_provider!(Creep,);

impl Default for Creep {
    fn default() -> Self {
        Self {
            sprite: Handle::NONE,
            move_speed: 100.0,
        }
    }
}

impl TypeUuidProvider for Creep {
    // Returns unique script id for serialization needs.
    fn type_uuid() -> Uuid {
        uuid!("c5671d19-9f1a-4286-8486-add4ebaadaac")
    }
}

fn pub_mqtt_loop(
    server_addr: String,
    server_port: String,
    rx1: Receiver<MqttMsg>,
    client_id: String,
) -> Result<Receiver<PlayerData>, Error> {
    let (tx, rx): (Sender<PlayerData>, Receiver<PlayerData>) = bounded(10000);
    let server_addr = server_addr.clone();
    let server_port = server_port.clone();
    let rx1 = rx1.clone();
    thread::spawn(move || -> Result<(), Error> {
        let update = tick(Duration::from_millis(100));
        let mut msgs: Vec<MqttMsg> = vec![];
        loop {
            let tx = tx.clone();
            let (mut mqtt2, mut connection) = create_mqtt_client(
                server_addr.clone(),
                server_port.clone(),
                generate_client_id(),
                true,
            )?;
            let (btx, brx): (Sender<bool>, Receiver<bool>) = bounded(10);
            thread::spawn(move || {
                let rex_td = Regex::new(r"td/([\w\S]+)/client").unwrap();
                for (i, notification) in connection.iter().enumerate() {
                    thread::sleep(Duration::from_millis(1));
                    if let Ok(x) = notification {
                        if let rumqttc::Event::Incoming(x) = x {
                            if let rumqttc::v4::Packet::Publish(x) = x {
                                let handle = || -> Result<(), Error> {
                                    let payload = x.payload;
                                    let msg = match std::str::from_utf8(&payload[..]) {
                                        Ok(msg) => msg,
                                        Err(err) => {
                                            return Err(failure::err_msg(format!(
                                                "Failed to decode publish message {:?}",
                                                err
                                            )));
                                            //continue;
                                        }
                                    };
                                    let topic_name = x.topic.as_str();
                                    let vo: serde_json::Result<PlayerData> =
                                        serde_json::from_str(&msg);
                                    if let Ok(v) = vo {
                                        tx.try_send(v);
                                    } else {
                                        warn!("Json Parser error");
                                    };
                                    Ok(())
                                };
                                if let Err(msg) = handle() {
                                    println!("{:?}", msg);
                                    continue;
                                }
                            }
                        }
                    } else {
                        btx.try_send(true).unwrap();
                        break;
                    }
                }
            });
            loop {
                select! {
                    recv(update) -> d => {
                        let mut i: usize = 0;
                        loop {
                            if msgs.len() <= i {
                                break;
                            }
                            let diff = msgs[i].time.duration_since(SystemTime::now());
                            let mut difftime = 0;
                            match diff {
                                Ok(n) => { difftime = n.as_micros(); },
                                Err(_) => {},
                            }
                            if difftime == 0 {
                                let msg_res = mqtt2.publish(msgs[i].topic.clone(), QoS::AtMostOnce, false, msgs[i].msg.clone());
                                match msg_res {
                                    Ok(_) =>{},
                                    Err(x) => {
                                        warn!("??? {}", x);
                                    }
                                }
                                msgs.remove(i);
                            } else {
                                i += 1;
                            }
                        }
                    },
                    recv(brx) -> d => {
                        break;
                    },
                    recv(rx1) -> d => {
                        let handle = || -> Result<(), Error>
                        {
                            if let Ok(d) = d {
                                let diff = d.time.duration_since(SystemTime::now());
                                let mut difftime = 0;
                                match diff {
                                    Ok(n) => { difftime = n.as_micros(); },
                                    Err(_) => {},
                                }
                                if d.topic.len() > 2 {
                                    if difftime == 0 {
                                        let msg_res = mqtt2.publish(d.topic.clone(), QoS::AtMostOnce, false, d.msg.clone());
                                        match msg_res {
                                            Ok(_) =>{},
                                            Err(x) => {
                                                warn!("Failed {:?}", d);
                                                msgs.push(d);
                                            }
                                        }
                                    } else {
                                        msgs.push(d);
                                    }
                                }
                            }
                            Ok(())
                        };
                        if let Err(msg) = handle() {
                            warn!("mqtt {:?}", msg);
                            break;
                        }
                    }
                }
            }
            thread::sleep(Duration::from_millis(100));
        }
        Ok(())
    });
    Ok(rx)
}
